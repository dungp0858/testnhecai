﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyNhaHangPIZZA
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.DanhSachDT();
        }
// Hiển thị Danh sách pizza sắp xếp theo thứ tự tăng dần 
        public void DanhSachDT()
        {
            try
            {
                SqlConnection con = new SqlConnection("Data Source=DESKTOP-756ECCQ;Initial Catalog=QuanLyNhaHangPizza;Integrated Security=True");
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[PIZZA_PRO] ORDER BY [ID_SP] ASC ", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable data = new DataTable();
                da.Fill(data);
                danhsachsanpham.DataSource = data;

                return;
            }
            catch (Exception)
            {
                
            }
        }
// End
// Thêm sản phẩm pizza
        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-756ECCQ;Initial Catalog=QuanLyNhaHangPizza;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[PIZZA_PRO]([ID_SP],[NAME],[DESCRIPTION],[PRICE],[STATUS]) VALUES (@ID_SP,@NAME,@DESCRIPTION,@PRICE,@STATUS)", con);
            cmd.Parameters.AddWithValue("@ID_SP",int.Parse( txtID.Text));
            cmd.Parameters.AddWithValue("@NAME",txtName.Text);
            cmd.Parameters.AddWithValue("@DESCRIPTION",txtDescription.Text);
            cmd.Parameters.AddWithValue("@PRICE",decimal.Parse(txtPrice.Text));
            cmd.Parameters.AddWithValue("@STATUS", txtStatus.Text);
            cmd.ExecuteNonQuery();
            con.Close();
            MessageBox.Show("Thêm Pizza thành công :)) ");
            loadtext();
            DanhSachDT();
        }
//End

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        public void loadtext()
        {
            txtID.Text = "";
            txtName.Text = "";
            txtDescription.Text = "";
            txtPrice.Text = "";
            txtStatus.Text = "";
            txtSearch.Text = "";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            try
            {
                SqlConnection con = new SqlConnection("Data Source=DESKTOP-756ECCQ;Initial Catalog=QuanLyNhaHangPizza;Integrated Security=True");
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[PIZZA_PRO] WHERE ID_SP=@ID_SP ", con);
                cmd.Parameters.AddWithValue("@ID_SP",int.Parse(txtSearch.Text));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable data = new DataTable();
                da.Fill(data);
                danhsachsanpham.DataSource = data;
                loadtext();
                return;
            }
            catch (Exception)
            {

            }
        }
    }
}
